"""
Meant to extend extract_style_exposures (though future versions may broadly extend other code).
"""


import extract_style_exposures
import extract_risk_decomp
import extract_position_reports
import os
import pandas as pd


def style_multifolder_extraction(main_folder: str, output_folder: str, subfolders: list = None, single_file_name: str = None,
                         sheet_names: list = None, multiple_file_names: list = None, file_name_prefix: str = '',
                         file_name_suffix: str = ''):
    """
    Function to call extract_style_exposures script.

    Either single_file_name or multiple_file_name should be provided, else the list of dataframes is returned.
    """
    if subfolders is None:
        subfolders = list(os.walk(main_folder))[0][1]

    all_dataframes = []
    for folder in subfolders:
        all_dataframes.append(extract_style_exposures.data_extraction(os.path.join(main_folder, folder),
                                                                      sheet_name='Style exposure breakdown_ARames', extension='.xlsx'))

    if single_file_name is None and multiple_file_names is None:
        return all_dataframes
    elif single_file_name is None:
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(os.path.join(output_folder, file_name_prefix + multiple_file_names[ind] +
                                                      file_name_suffix))
    else:
        writer = pd.ExcelWriter(os.path.join(output_folder, file_name_prefix +  single_file_name + file_name_suffix),
                                engine='xlsxwriter')
        if sheet_names is None:
            sheet_names = ['Sheet'+str(ind+1) for ind in range(len(subfolders))]
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(writer, sheet_name=sheet_names[ind])
        writer.save(), writer.close()


def risk_decomp_multifolder_extraction(main_folder: str, output_folder: str, subfolders: list = None, single_file_name: str = None,
                         sheet_names: list = None, multiple_file_names: list = None, file_name_prefix: str = '',
                         file_name_suffix: str = ''):
    """
    Function to call extract_risk_decomp script. Currently incredibly inefficient for large operations.

    Either single_file_name or multiple_file_name should be provided, else the list of dataframes is returned.
    """

    # Calling column and row extraction in separate functions is very inefficient.
    # TODO:
    # 1) Either change the underlying function to accept file pointers somehow so I/O operations are reduced.
    # 2) Or make the whole thing concurrent / multiprocess so it can work much faster.

    if subfolders is None:
        subfolders = list(os.walk(main_folder))[0][1]

    all_dataframes = []
    default_options = {'columns_to_extract': ['Portfolio Risk', 'Benchmark Risk'],
                        'rows_to_extract': ['Common Factor Risk', 'Selection Risk'],
                        'sheet_name': 'MAC.L Grouping_ARamesh'}
    for folder in subfolders:
        f_path = os.path.join(main_folder, folder)
        default_options['file_list'] = list(os.walk(f_path))[0][2]
        df = extract_risk_decomp.extract_one_column(f_path, **default_options).merge(
            extract_risk_decomp.extract_one_row(f_path, **default_options), how='outer', left_index=True, 
            right_index=True)
        all_dataframes.append(df)

    if single_file_name is None and multiple_file_names is None:
        return all_dataframes
    elif single_file_name is None:
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(os.path.join(output_folder, file_name_prefix + multiple_file_names[ind] +
                                                      file_name_suffix))
    else:
        writer = pd.ExcelWriter(os.path.join(output_folder, file_name_prefix +  single_file_name + file_name_suffix),
                                engine='xlsxwriter')
        if sheet_names is None:
            sheet_names = ['Sheet'+str(ind+1) for ind in range(len(subfolders))]
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(writer, sheet_name=sheet_names[ind])
        writer.save(), writer.close()


def position_report_multifolder_extraction(main_folder: str, output_folder: str, subfolders: list = None, single_file_name: str = None,
                         sheet_names: list = None, multiple_file_names: list = None, file_name_prefix: str = '',
                         file_name_suffix: str = ''):
    """
    Function to call extract_risk_decomp script. Currently incredibly inefficient for large operations.

    Either single_file_name or multiple_file_name should be provided, else the list of dataframes is returned.
    """

    # Calling column and row extraction in separate functions is very inefficient.
    # TODO:
    # 1) Either change the underlying function to accept file pointers somehow so I/O operations are reduced.
    # 2) Or make the whole thing concurrent / multiprocess so it can work much faster.

    if subfolders is None:
        subfolders = list(os.walk(main_folder))[0][1]

    all_dataframes = []
    default_options = {'sheet_name': 'Positions_Zhan_Contr to TE snap'}
    for folder in subfolders:
        f_path = os.path.join(main_folder, folder)
        default_options['file_list'] = list(os.walk(f_path))[0][2]
        df = extract_position_reports.data_extraction(f_path, **default_options)
        all_dataframes.append(df)

    if single_file_name is None and multiple_file_names is None:
        return all_dataframes
    elif single_file_name is None:
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(os.path.join(output_folder, file_name_prefix + multiple_file_names[ind] +
                                                      file_name_suffix))
    else:
        writer = pd.ExcelWriter(os.path.join(output_folder, file_name_prefix +  single_file_name + file_name_suffix),
                                engine='xlsxwriter')
        if sheet_names is None:
            sheet_names = ['Sheet'+str(ind+1) for ind in range(len(subfolders))]
        for ind in range(len(all_dataframes)):
            all_dataframes[ind].to_excel(writer, sheet_name=sheet_names[ind])
        writer.save(), writer.close()


def main():
    main_folder = r'C:\Users\ramesa1\OneDrive - Invesco\Work-NYCVMPRRAMESA11\Main Street funds\Quarterly Portfolio Review\July 2021\Historical sector position reports'
    output_folder = main_folder
    subfolders = ['253124', '253125', '253128', '253129', '253143']
    single_file_name = 'Sector Contr to TE aggregated.xlsx'
    sheet_names = ['All Cap', 'Main Street', 'Mid Cap', 'Small Cap', 'Rising Dividends']

    # style_multifolder_extraction(main_folder, output_folder, subfolders=subfolders, single_file_name=single_file_name, 
    # sheet_names=sheet_names)
    # risk_decomp_multifolder_extraction(main_folder, output_folder, subfolders=subfolders, single_file_name=single_file_name,
    # sheet_names=sheet_names)
    position_report_multifolder_extraction(main_folder, output_folder, subfolders=subfolders, single_file_name=single_file_name,
    sheet_names=sheet_names)


if __name__ == '__main__':
    main()
