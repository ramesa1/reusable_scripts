"""
Only meant to extract what is needed from a single folder containing all the exported outputs. For multiple folder
usage, this needs to be extended.
"""


import pandas as pd
import os


def data_extraction(path: str, sheet_name: str, file_list: list = None, style_rows: list = None,
                    output_file: str = None, extension: str = '.xls'):
    """
    Data Extraction function.

    Arguments:
        path : Path of the folder containing the files with data.
        sheet_name : Name of the sheet to read in the files inside path. Assume same sheet name for all files.
        file_list: Optional; Provide list of file names directly. If not provided, function reads file list directly
                            from the folder.
        style_rows: Optional; Provide list of style factor rows to extract. If not provided, default list is given which
                            should match both GEMLTL and MAC.L factors.
        output_file: Optional; Full path of output file. If omitted, assume no output file needs to be created and
                            simply return the full dataframe.
        extension: Optional; Meant to give margins to understand filename. Please include the period delimiter manually
                            for eg: ".xls" or ".xlsx". Typically date is extracted from the filename under the
                            assumption that it is the [end-x-8, end-x-4) characters in the file_name string where x is
                            the length of extension string including the period delimiter.
    """

    # Extensions:
    # Ignore temp files. Maybe they start with a '~'? Not sure.
    # Allow for extraction of portfolio and benchmark exposure as well

    if style_rows is None:
        style_rows = ['Beta', 'Dividend Yield', 'Earnings Quality', 'Earnings Variability', 'Earnings Yield',
                         'Growth', 'Investment Quality', 'Leverage', 'Liquidity', 'Long-Term Reversal',
                         'Mid Capitalization', 'Momentum', 'Other Local', 'Profitability', 'Residual Volatility',
                         'Size', 'Value']

    if file_list is None:
        file_list = list(os.walk(path))[0][2]

    values = []
    for file_name in file_list:
        df = pd.read_excel(os.path.join(path, file_name), sheet_name=sheet_name, header=[0, 1], index_col=0).\
            loc[style_rows, ('Exposure', 'Active')].rename(file_name[-(len(extension)+8):-len(extension)])
        values.append(df)

    df = pd.DataFrame(values)
    df.index = pd.to_datetime(df.index)

    if output_file is None:
        return df
    else:
        df.to_excel(output_file)


def main():
    path = r'C:\Users\ramesa1\Documents\Work\Main Street funds\Risk Model Comparison\Data\style exposure exports\253124'
    # file_list = list(os.walk(path))[0][2]
    output_file = r'C:\Users\ramesa1\Documents\Work\Main Street funds\Risk Model Comparison\Data\style exposure exports\All cap.xlsx'

    default_options = {'style_rows': ['Beta', 'Dividend Yield', 'Earnings Quality', 'Earnings Variability',
                                         'Earnings Yield', 'Growth', 'Investment Quality', 'Leverage', 'Liquidity',
                                         'Long-Term Reversal', 'Mid Capitalization', 'Momentum', 'Other Local',
                                         'Profitability', 'Residual Volatility', 'Size', 'Value'],
                       'sheet_name': 'Style exposure breakdown with a', 'output_file': output_file}
    del default_options['style_columns']

    data_extraction(path, **default_options)


if __name__ == '__main__':
    main()
