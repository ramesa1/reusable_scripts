"""
Only meant to extract what is needed from a single folder containing all the exported outputs. For multiple folder
usage, this needs to be extended.
"""


import pandas as pd
import os


def data_extraction(path: str, sheet_name: str, file_list: list = None, output_file: str = None, extension: str = '.xlsx'):
    """
    Data Extraction function.

    Arguments:
        path : Path of the folder containing the files with data.
        sheet_name : Name of the sheet to read in the files inside path. Assume same sheet name for all files.
        file_list: Optional; Provide list of file names directly. If not provided, function reads file list directly
                            from the folder.
        output_file: Optional; Full path of output file. If omitted, assume no output file needs to be created and
                            simply return the full dataframe.
        extension: Optional; Meant to give margins to understand filename. Please include the period delimiter manually
                            for eg: ".xls" or ".xlsx". Typically date is extracted from the filename under the
                            assumption that it is the [end-x-8, end-x-4) characters in the file_name string where x is
                            the length of extension string including the period delimiter.
    """

    # Extensions:
    # Ignore temp files. Maybe they start with a '~'? Not sure.
    # Allow for extraction of portfolio and benchmark exposure as well

    if file_list is None:
        file_list = list(os.walk(path))[0][2]

    values = []
    for file_name in file_list:
        df = pd.read_excel(os.path.join(path, file_name), sheet_name=sheet_name, index_col=0).\
            loc['Communication Services':, '%CR to Active Total Risk'].rename(file_name[-(len(extension)+8):-len(extension)])
        
        values.append(df)

    df = pd.DataFrame(values)
    df.index = pd.to_datetime(df.index)

    if output_file is None:
        return df
    else:
        df.to_excel(output_file)


def main():
    path = r'C:\Users\ramesa1\OneDrive - Invesco\Work-NYCVMPRRAMESA11\Main Street funds\Quarterly Portfolio Review\July 2021\Historical sector position reports\253125'
    # file_list = list(os.walk(path))[0][2]
    output_file = r'C:\Users\ramesa1\OneDrive - Invesco\Work-NYCVMPRRAMESA11\Main Street funds\Quarterly Portfolio Review\July 2021\Historical sector position reports\Main Street test.xlsx'

    default_options = {'sheet_name': 'Positions_Zhan_Contr to TE snap', 'output_file': output_file}

    data_extraction(path, **default_options)


if __name__ == '__main__':
    main()
