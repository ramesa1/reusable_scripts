from ivz_risk_shared import PeerDataClient
from ivz_risk_shared.quanthub import QuantHubClient
from ivz_risk_shared.peerfunddata import PeerGroupData
from ivz_risk_shared.factordata import FactorBuilder
from ivz_risk_shared.fund import PeerGroup, Fund
from ivz_risk_shared.reportconfigs import IARptConfig, RptCfgClient
from ivz_risk_shared.PeerDataClient import PeerDataClient

import pandas as pd
from os.path import join
from datetime import date
from dateutil.relativedelta import relativedelta

import warnings
warnings.filterwarnings('ignore')

def quantile_exc(ser, q):
    ser_sorted = ser.sort_values()
    rank = q * (len(ser) + 1) - 1
    assert rank > 0, 'quantile is too small'
    rank_l = int(rank)
    return ser_sorted.iat[rank_l] + (ser_sorted.iat[rank_l + 1] - ser_sorted.iat[rank_l]) * (rank - rank_l)

class FundHoldings(PeerGroup): 
    @property
    def fund_returns(self):
        return pd.concat([fund.returns for fund in self.members_dict.values()], axis=1)

    @property
    def position_info(self):
        concat_list = []
        for fund in self.members_dict.values():
            try: 
                fund_info = getattr(fund,'info')
                concat_list.append(fund_info)
            except:
                pass
        if len(concat_list > 0):
            return pd.concat(concat_list, axis=1)
        else:
            return pd.DataFrame()

    def add_position_data(self, info, returns):
        try:
            members_dict = getattr(self,'members_dict')
        except:
            members_dict = {}
        for col in info.columns:
            # Logic: if a fund has no returns, do not create a fund obj
            # Each Fund object requires an id and a name
            # Three properties are legacy properties, caught in try/except blocks
            if col not in returns.columns:
                pass
            else:
                try:
                    fund_obj = FundHolding(col, info.loc['portname', col])
                except:
                    fund_obj = FundHolding(col, info.loc['name', col])
                try:
                    fund_obj.fundfamily = info.loc['fundfamily', col]
                except:
                    pass
                try:
                    fund_obj.shareclass = info.loc['shareclass', col]
                except:
                    pass
                try:
                    fund_obj.category = info.loc['category', col]
                except:
                    pass
                fund_obj.info = info[col].copy()
                fund_obj.returns = returns[[col]]
                members_dict[col] = fund_obj
        self.members_dict = members_dict
        self.members = list(self.members_dict.values())
        self.regMembers = list(self.members_dict.keys())

class FundHolding(Fund):
    def __init__(self, id, name, fundfamily = None, shareclass = None, category = None):
        self.fundid = id
        self.portname = name
        if fundfamily:
            self.fundfamily = fundfamily
        if shareclass:
            self.shareclass = shareclass         
        if category:
            self.category = category
        self.model_dict = {}

    def some_func(self):
        pass


def rename_columns(column_names):
    column_rename_map = {
        r'(\w+)VAL(\w+)': 'Value Factor'
        , r'(\w+)EQW(\w+)': 'Size Factor'
        , r'(\w+)MOM(\w+)': 'Momentum Factor'
        , r'(\w+)VOL(\w+)': 'Low Volatility Factor'
        , r'(\w+)QUAL(\w+)': 'Quality Factor'
    }
    dont_rename_columns = ['Fund Name', 'Fund Ticker', 'Fund SecId', 'positionType', 'SecurityName', 'gicsSector', 'Weight', 'Position Alpha 1M', 'Position Alpha 3M', 'R-sq - Security', 'SO VaR84 5y', 'SO VaR95 5y', 'SO VaR99 5y', 'Date']
    
    column_names = pd.Series(column_names).replace(to_replace=None, regex=column_rename_map)

    bm_col_excl_cond = ~column_names.isin(dont_rename_columns) & ~column_names.str.contains('Factor')
    column_names[bm_col_excl_cond] = column_names[bm_col_excl_cond].replace(regex={'^(\w+)': 'Benchmark Factor'})

    return column_names


def main_func(output_location: str, start_date: str=date(date.today().year, date.today().month, 1)-relativedelta(years=12, days=1), end_date: str=date(date.today().year, date.today().month, 1)-relativedelta(days=1), fundlist: list=None):
    if fundlist is None:
        cu = RptCfgClient().cfgs
        fundlist = [ cu[k]['ivz_fund'] for k in cu if 'ivz_fund' in cu[k] ]

        # remove duff funds as these seem to cause the code to crash
        fundlist = [x for x in fundlist if len(x)<6]

    tot_funds = len(fundlist)

    df_all_data = pd.DataFrame()

    fb = FactorBuilder()
    pdc = PeerDataClient()
    fund_count = 0
    for xfund in fundlist:
        fund_id = xfund
        fund_count = fund_count + 1    
        config = IARptConfig('IA_'+fund_id).as_dict()
        
        daterange = pd.date_range(start_date, end_date , freq='1M')
        
        
        date_count = 0
        tot_dates = len(daterange)

        try:
            pos_info = pdc.getFundPos(fund_id)
            pos_returns = pdc.getFundPosSims(fund_id)
        except:
            try:
                alt_id = pdc.masterlist[pdc.masterlist['SecId'] == fund_id]['FundId'].iloc[0]
                pos_info = pdc.getFundPos(alt_id)
                pos_returns = pdc.getFundPosSims(alt_id)
            except:
                pos_info = []
                print(f'No data for {fund_id}')
        finally:
            try:
                nav = float(pos_info['ivz_nav'].iloc[0])
                pos_pv = pos_info['PV'].astype('float64')
                weights = pos_pv.div(nav).mul(100).rename('Weight')
        
                factors = fb.build_factors_default(config['facpick'], weekly = True)
            except Exception as e:
                print(f"Error on {fund_id}")
                print(e)
        
        sim_info = pos_info.copy()
        sim_info['name'] = sim_info.index
        sim_info = sim_info.T
        sim_returns = pos_returns.copy()
        sim_returns = sim_returns.mul(100)
        sim_returns.index = pd.to_datetime(sim_returns.index)

        pos_peers = FundHoldings()
        pos_peers.add_position_data(sim_info, sim_returns)
        pos_peers.add_factors(factors)
        pos_peers.setup_benchmark(factors[config['benchmark']])

        standalone_returns = pos_returns.mul(nav).div(pos_pv).mul(100)
        sec_peers = FundHoldings()
        so_returns = standalone_returns.copy()
        so_returns.index = pd.to_datetime(so_returns.index)

        eq_0 = so_returns.sum().eq(0)
        eq_0 = eq_0[eq_0].index

        sec_peers.add_position_data(sim_info, so_returns.drop(columns = eq_0))
        sec_peers.add_factors(factors)
        sec_peers.setup_benchmark(factors[config['benchmark']])

        for end_date in daterange:
            date_count = date_count + 1 
            end = pd.to_datetime(end_date).strftime("%m/%d/%Y")
            start = (pd.to_datetime(end) - pd.tseries.offsets.MonthEnd(24)).strftime("%m/%d/%Y")
            pct = (fund_count * date_count) / (tot_funds * tot_dates)
            print(f'Running {fund_id}' + ' ' + end + ': Fund ' + str(fund_count) + '/' + str(tot_funds) + ', Date ' + str(date_count) + '/' + str(tot_dates) + ', ' + f"{pct:.1%}") 
            
            #print (f"{pct:.0%}")
            #print("Getting data")
            
            datedict = {
                'begest': start,
                'endest': end,
                'begpred': start,
                'endpred': end
            }

            # Construct "Peer Group" of position sims and run OLS
            #print("Preparing sim data")

            pos_peers.datedict = datedict
            
            sec_peers.datedict = datedict

            print('Running OLS')
            pos_peers.fitOLSAll('residual')
            sec_peers.fitOLSAll('residual')

            print('Constructing table')
            position_alpha = pos_peers.resOLS.add(pos_peers.constOLS.iloc[:,0])

            # New position beta logic
            fac_rtns = factors[start:end].drop(columns = [config['benchmark']])
            fac_betas = pos_peers.parsOLS.T.drop(columns = [config['benchmark']]).sum()
            adj_bm_beta = pos_peers.parsOLS.T[config['benchmark']].add(1).sum() - 1 
            bm_returns = factors[config['benchmark']][start:end]
            position_beta = bm_returns.mul(adj_bm_beta) + fac_rtns.mul(fac_betas).sum(axis=1)

            pos_exp = pos_peers.parsOLS.T.copy()
            pos_exp[config['benchmark']] = pos_exp[config['benchmark']].add(1)

            pos_alpha_1m = position_alpha.tail(4).sum().rename("Position Alpha 1M")
            pos_alpha_3m = position_alpha.tail(12).sum().rename("Position Alpha 3M")

            rsq_ser = pd.Series([f.iloc[0] for f in sec_peers.rsqsOLS['rsq']],index = sec_peers.rsqsOLS.index, name = 'R-sq - Security')

            so_exposures = sec_peers.parsOLS.T
            so_exposures.columns = [col+' SO' for col in so_exposures.columns]

            print('calc VaR')
            var84_5y = standalone_returns.tail(260).apply(quantile_exc, args = (0.1587,)).mul(52**0.5).rename('SO VaR84 5y')
            var95_5y = standalone_returns.tail(260).apply(quantile_exc, args = (0.05,)).mul(52**0.5).rename('SO VaR95 5y')
            var99_5y = standalone_returns.tail(260).apply(quantile_exc, args = (0.01,)).mul(52**0.5).rename('SO VaR99 5y')

            info_cols = ['positionType','SecurityName','gicsSector']

            print('creating output')
            data_table = pd.concat([
                pos_info[info_cols],
                weights, 
                pos_exp, 
                pos_alpha_1m, 
                pos_alpha_3m, 
                rsq_ser, 
                var84_5y, 
                var95_5y, 
                var99_5y,
                so_exposures],axis = 1).sort_values(by = ['positionType','gicsSector','SecurityName'])

            data_cols = list(data_table.columns)
            try:
                fundmeta = pdc.masterlist[pdc.masterlist['Ticker'] == fund_id].iloc[0]
            except:
                fundmeta = pdc.masterlist[pdc.masterlist['SecId'] == fund_id].iloc[0]
            
            print('renaming columns') 
            data_table['Fund Name'] = fundmeta['Name']
            data_table['Fund Ticker'] = fundmeta['Ticker']
            data_table['Fund SecId'] = fundmeta['SecId']
            data_table = data_table[['Fund Name','Fund Ticker','Fund SecId'] + data_cols]
            data_table['Date'] = end_date 
            data_table.columns = rename_columns(data_table.columns)

            print('appending')
            df_all_data = df_all_data.append(data_table)

    if fundlist is None:
        filename = 'All Funds Stock Level IA over time.xlsx'
    elif len(fundlist) > 1:
        filename = 'Select Funds Stock Level IA over time.xlsx'
    else:
        filename = fundlist[0] + ' Stock Level IA over time.xlsx'
    df_all_data.to_excel(join(output_location, filename))
