import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from os.path import join


def create_boxplot(input_location, output_location, fund_code=None):
    df = pd.read_excel(input_location, parse_dates=['Date']).replace(r'', np.nan)

    # pull out ticker into its own column
    df.insert(2, 'Short Name', df['SecurityName'].str.rsplit(' ',1).apply(lambda x: x[1]))

    # Also move date col which isn't in every .pkl
    df.insert(1, 'Date', df.pop('Date'))

    # melt all cols after the "weight" col - which seems to change place
    cols = list(df.columns)
    weight_col = df.columns.get_loc('Weight')
    keep_cols = cols[:weight_col+1]


    df_melt = pd.melt(df, id_vars=keep_cols, var_name = 'Item', value_name='Value' )
    df_melt = df_melt[df_melt['Value'].notna()].iloc[:-1] # get rid of all the factors that aren't relevant for the particular fund


    # tip: count the characters and that's then the "to" number below
    def extra_cols(field):
        if field[:6] == 'SO VaR':
            return 'VaR'
        elif (field [:4] == 'R-sq') or (field [:7] == 'R-Sq SO'):
            return 'R-Sq'
        elif field [-1] == 'd':
            return 'Factor Contribution'
        elif field[:14] == 'Position Alpha':
            return 'Forward Alpha'
        elif field[-2:] == 'SO':
            return 'Factor Exposure'
        else:
            return 'Factor Contribution'


    df_melt['Item Type'] = df_melt['Item'].apply(extra_cols)

    # data for all factor exposures
    wanted_list = ['Factor Exposure', 'R-Sq']
    if fund_code is None:
        df_factor = df_melt[df_melt['Item Type'].isin(wanted_list) & (df_melt['positionType'] == 'Equity') & (df_melt['Weight']>0)]
    else:
        df_factor = df_melt[df_melt['Item Type'].isin(wanted_list) & (df_melt['Fund Ticker']==fund_code) & (df_melt['positionType'] == 'Equity') & (df_melt['Weight']>0)]

    # df_main = df_factor

    Sec_list=df.loc[(df['Date']==df['Date'].max()) & (df['positionType']=='Equity'), :].sort_values('R-sq - Security', ascending=False)['Short Name'].unique()
    l=np.array_split(Sec_list, 3)
    df_factor.loc[:, 'Short Name'] = df_factor.loc[:, 'Short Name'].astype(pd.CategoricalDtype(Sec_list, ordered=True))

    df_factor.loc[df_factor['Item'].str.contains(' SO'), 'Item']  = df_factor.loc[df_factor['Item'].str.contains(' SO'), 'Item'].str[:-3]

    factors = list(df_factor['Item'].unique())
    num_facs = len(df_factor['Item'].unique())

    for ind in range(len(l)):
    # for ind in range(1):
        plt.figure(figsize=(20,10))
        df_factor_ = df_factor[df_factor['Short Name'].isin(l[ind])]

        for n, fact in enumerate(factors):
            # add a new subplot iteratively
            ax = plt.subplot(1, num_facs, n + 1)

            # filter df and plot ticker on the new subplot axis
            df_factor__ = df_factor_[(df_factor_['Item'] == fact)].sort_values('Short Name', ascending=False)
            sns.boxplot(x='Value', y='Short Name', color='white', width=0.4, fliersize=0, data=df_factor__, order=l[ind], ax=ax)
            sns.swarmplot(x='Value', y='Short Name', color='green', data=df_factor__[df_factor__['Date']==df_factor__['Date'].max()], order=l[ind], ax=ax)

            # create symmetric x-axis
            if fact == 'R-sq - Security':
                used_range = (0,1)
            else:
                temp = max(abs(max(df_factor_[df_factor_['Item'] == fact]['Value'])),abs(min(df_factor_[df_factor_['Item'] == fact]['Value']))) + 0.5
                used_range = (-temp, temp)
                #print (fact , used_range)
                #used_range = (-xrange, xrange)
            ax.grid()
            ax.set(title = fact, ylabel='', xlabel = '', xlim = used_range, frame_on=True)

        plt.subplots_adjust(wspace=0.5)    
        # plt.suptitle(str(Funds) + ' ' + str(wanted_list))
        plt.savefig(join(output_location, str(ind)+'.png'), facecolor='w')
        # plt.show()
