# equity risk library
import sys
sys.path.append(r'C:\Users\ramesa1\OneDrive - Invesco\Code\equity_risk_library')
import barra_api_wrapper
import barra_bdt_wrapper
import snowflake_querier
from helper_functions_general import get_random_suffix_prefix_string
from helper_functions_analysis import id_cleanup

# inbuilt libraries
from datetime import date
from multiprocessing.pool import ThreadPool
from itertools import repeat
from functools import reduce

# third party libraries
import pandas as pd
import numpy as np

# vendor libraries
from msci.bdt.context.ServiceClient import ServiceClient


def combine_portfolios_reupload(bdt_service_client: ServiceClient, portfolios_benchmarks: list[tuple], upload_date: str, model: str = 'MAC.L', idtype_override: dict = None, extra_ids: dict = None, upload_portfolio_name: str = 'temp pfolio') -> pd.DataFrame | None:
    """Function to combine multiple portfolios together and reupload them as a temporary portfolio to BarraOne. This is useful when you want to combine a fund and benchmark into one big portfolio to run a report with, or get asset level data for the union of several portfolios etc. You specify the portfolio using a barra tuple (as defined in the wrapper code) but if you do not want to include a benchmark, you can specify 'CASH' as the benchmark name. The function will simply pass your tuple as portfolios whose positions reports have to be retrieved from BarraOne.

    The function does the following operations in sequence:
    1. Download a positions report of the portfolios for the given date
    2. Extract the IDs and upload it back as a temporary portfolio to Barra with the original portfolio weights (bench holdings get uploaded as 0 weight holdings in the portfolio correctly).

    Args:
        bdt_service_client (ServiceClient): ServiceClient object returned by barra_login_client.
        portfolios_benchmarks (list[tuple]): Usual portfolio_benchmark tuple format specified in the wrapper code. An example of this argument is ('253116', 'InvPROD', 'NONE', {'BenchmarkName': 'MSAXUSSMCD', 'BenchmarkOwner': 'SYSTEM'})
        model (str, optional): Risk model to use. Defaults to 'MAC.L'.
        idtype_override (dict, optional): Optional argument in case the LOCALID override for bad IDs in the function below is not sufficient. The format of this dict is {id: idtype}. Defaults to None.
        extra_ids (dict, optional): Optional argument to add extra IDs to the positions data. The format of this dict is {id: idtype}. Defaults to None.

    Returns:
        pd.DataFrame | None: Returns the covariance matrix as a DataFrame. If the upload fails, returns None. In future versions, this will be changed to raise an exception but for now, calling code has to handle it manually.
    """

    positions_data = barra_api_wrapper.create_new_export_set(bdt_service_client, 'ReuploadTempExp ' + get_random_suffix_prefix_string(), portfolios_benchmarks, [('Positions_port_info_AR', 'ARamesh')], model, temporary=True, run_options={'analysis_date': upload_date, 'return_only_report_data': False})

    positions_data = [positions_data[0][i] for i in range(len(positions_data[0]))]

    positions_data = barra_api_wrapper.rearrange_exp_report_order(positions_data, required_order=[(p[0], ) for p in portfolios_benchmarks], dimensions=['Portfolio'])

    positions_data = pd.concat([p.drop(0, axis=0) for p in positions_data])
    positions_data['Asset ID'] = positions_data['SEDOL'].where(positions_data['SEDOL'] != "N/A", positions_data['Asset ID'])

    to_upload = positions_data[['Asset ID', 'Weight (%)']].set_axis(['id', 'Weight'], axis=1).assign(**{'idtype': 'SEDOL'})
    bad_ids = (positions_data['SEDOL'] == "N/A")
    to_upload.loc[bad_ids, 'idtype'] = positions_data.loc[bad_ids, 'Asset ID Type']

    if idtype_override is not None:
        for id in idtype_override:
            to_upload.loc[to_upload['id']==id, 'idtype'] = idtype_override[id]

    if extra_ids is not None:
        to_upload = pd.concat([to_upload, pd.DataFrame({'id': extra_ids.keys(), 'idtype': extra_ids.values(), 'Weight': [0]*len(extra_ids)})], ignore_index=True)

    _, import_detail = barra_api_wrapper.upload_to_barra(to_upload, upload_portfolio_name, upload_date.strftime('%Y-%m-%d'), bdt_service_client)
    if not (pd.DataFrame(import_detail)['ResultCode']=='OK').all():
        print('Failed to reupload positions data to BarraOne')
        return False
    
    return True


def get_covar_matrix(bdt_service_client: ServiceClient, covar_date: date, portfolios_benchmarks: list[tuple], model: str = 'MAC.L', idtype_override: dict = None, extra_ids: dict = None) -> pd.DataFrame | None:
    """Function to get the covariance matrix for a given (portfolio + benchmark) combo for a given date. The function does the following operations in sequence:
    1. Download a positions report of the portfolio and benchmark for the given date
    2. Extract the IDs and upload it back as a temporary portfolio to Barra.
    3. Run a correlation report on the temporary portfolio to get the correl matrix. Delete the temporary portfolio once this is done.
    4. Calculate the covariance matrix using the correl matrix and the standard deviations of the assets in the portfolio and return this.

    Args:
        bdt_service_client (ServiceClient): ServiceClient object returned by barra_login_client.
        covar_date (date): Date for which the covariance matrix needs to be downloaded. Best to give a Friday.
        portfolios_benchmarks (list[tuple]): Usual portfolio_benchmark tuple format specified in the wrapper code. An example of this argument is ('253116', 'InvPROD', 'NONE', {'BenchmarkName': 'MSAXUSSMCD', 'BenchmarkOwner': 'SYSTEM'})
        model (str, optional): Risk model to use. Defaults to 'MAC.L'.
        idtype_override (dict, optional): Optional argument in case the LOCALID override for bad IDs in the function below is not sufficient. The format of this dict is {id: idtype}. Defaults to None.
        extra_ids (dict, optional): Optional argument to add extra IDs to the positions data. The format of this dict is {id: idtype}. Defaults to None.

    Returns:
        pd.DataFrame | None: Returns the covariance matrix as a DataFrame. If the upload fails, returns None. In future versions, this will be changed to raise an exception but for now, calling code has to handle it manually.
    """

    upload_success = combine_portfolios_reupload(bdt_service_client, portfolios_benchmarks, covar_date, model, idtype_override=idtype_override, extra_ids=extra_ids, upload_portfolio_name='temp portfolio')

    if not upload_success:
        return None
    
    positions_data = barra_api_wrapper.create_new_export_set(bdt_service_client, 'Correl Temp ExpSet 2', [('temp portfolio', bdt_service_client.__dict__['user_id'], 'NONE')], [('Positions_port_info_AR', 'ARamesh')], model, temporary=True, run_options={'analysis_date': covar_date, 'return_only_report_data': True})[0][0].drop(0, axis=0)
    positions_data['Asset ID'] = positions_data['SEDOL'].where(positions_data['SEDOL'] != "N/A", positions_data['Asset ID'])

    correlations = barra_api_wrapper.create_new_export_set(bdt_service_client, 'Correl Temp ExpSet 3', [('temp portfolio', bdt_service_client.__dict__['user_id'], 'NONE')], [('Asset Correlations Report', 'SYSTEM')], model, temporary=True, run_options={'analysis_date': covar_date, 'return_only_report_data': True})

    barra_api_wrapper.delete_portfolios(bdt_service_client, [('temp portfolio', 'ARamesh')])

    std_dev = positions_data[['Asset ID', 'Total Risk']].set_index('Asset ID').squeeze()

    covar = correlations[0][0].where(~correlations[0][0].isna(), 0).set_index(['Asset ID', 'Asset Name']).pipe(lambda df: df.set_axis(df.index, axis=1)).apply(lambda s: s * s.index.get_level_values(0).map(std_dev) * std_dev.loc[s.name[0]])

    return covar


def get_covar_matrix_advanced(bdt_service_client: ServiceClient, covar_date: date, portfolios_benchmarks: list[tuple], model: str = 'MAC.L', idtype_override: dict = None, extra_ids: dict = None, return_type: list = None, dont_scale_exposure: list = None) -> dict:
    """Function to get the covariance matrix for a given (portfolio + benchmark) combo for a given date. The function does the following operations in sequence:
    1. Reupload the positions from the fund and the benchmark as a temporary portfolio into barra. Also add in any new IDs given.
    3. Run a positions report on the temporary portfolio to get the positions data in case there are new holdings added in the reupload.
    4. Run a portfolio exposure report on the temporary portfolio to get the factor exposures. Discard the factor covariance and specific covariance.
    5. Delete the temporary portfolio.
    6. Get the factor covariances from snowflake using a table maintained by Mike M.
    7. Figure out common IDs between the factor exposures and the positions data using my id_cleanup function.
    8. Retrieve factor mapping from snowflake.
    9. Construct the various covariance matrices and return whatever the function call asked for.

    Args:
        bdt_service_client (ServiceClient): ServiceClient object returned by barra_login_client.
        covar_date (date): Date for which the covariance matrix needs to be downloaded. Best to give a Friday.
        portfolios_benchmarks (list[tuple]): Usual portfolio_benchmark tuple format specified in the wrapper code. An example of this argument is ('253116', 'InvPROD', 'NONE', {'BenchmarkName': 'MSAXUSSMCD', 'BenchmarkOwner': 'SYSTEM'})
        model (str, optional): Risk model to use. Defaults to 'MAC.L'.
        idtype_override (dict, optional): Optional argument in case the LOCALID override for bad IDs in the function below is not sufficient. The format of this dict is {id: idtype}. Defaults to None.
        extra_ids (dict, optional): Optional argument to add extra IDs to the positions data. The format of this dict is {id: idtype}. Defaults to None.
        return_type (list, optional): List of strings that specify what data to return. Defaults to None in which case it is changed to ['factor covariance', 'factor_exposures', 'specific_covariance'] and those 3 matrices are returned.
        Options available are 'all_factor_covariance', 'factor_covariance', 'factor_exposures', 'specific_covariance', 'stock_covariance', 'common_factor_covariance', 'currency_covariance', 'local_market_covariance', 'positions_report'.
        dont_scale_exposure (list, optional): List of IDs for which the exposure scaling should not be applied. Likely a useless argument, but leaving it in since it was implemented. Defaults to None.

    Returns:
        dict: Returns a dict with the keys as elements of return_type and the values as the corresponding data. If the upload fails, returns None.
    """

    if return_type is None:
        return_type = ['factor covariance', 'factor_exposures', 'specific_covariance']

    temp_covar_port_name = 'Temp Covar Portfolio ' + get_random_suffix_prefix_string()
    reuploaded_port_tuple = [(temp_covar_port_name, bdt_service_client.__dict__['user_id'], 'NONE', portfolios_benchmarks[0][3])]

    if return_type is None:
        return_type = ['factor covariance', 'factor_exposures', 'specific_covariance']

    upload_success = combine_portfolios_reupload(bdt_service_client, portfolios_benchmarks, covar_date, model, idtype_override=idtype_override, extra_ids=extra_ids, upload_portfolio_name=temp_covar_port_name)

    if not upload_success:
        print('Failed to reupload positions data to BarraOne to generate the covariance matrix. Check rejects on the website for more info.')
        return None
    
    positions_report = barra_api_wrapper.create_new_export_set(bdt_service_client, 'CovarMat Expset ' + get_random_suffix_prefix_string(), reuploaded_port_tuple, [('Positions_Zhan_Contr to TE snapshot_Stock', 'ARamesh')], temporary=True, run_options={'analysis_date':covar_date, 'return_only_report_data': True})[0][0]

    credentials_file = r'C:\Users\ramesa1\OneDrive - Invesco\Code\barraone api exploration\credentials.json'
    client, credentials = barra_bdt_wrapper.bdt_login_client_cred(credentials_file)

    factor_exposures, _, _ = barra_bdt_wrapper.portfolioExposureExpSet(reuploaded_port_tuple[0][:2], covar_date, client, credentials)

    barra_api_wrapper.delete_portfolios(bdt_service_client, reuploaded_port_tuple)

    id_columns = ['Asset ID', 'BARRAID', 'SEDOL', 'ISIN', 'CUSIP', 'COMPOSITEID']

    # correcting for an issue where barra randomly remaps what I'm uploading (with zero weight) to the wrong
    # instrument so it doesn't show up on the same line as the benchmark. removing those lines altogether. Also
    # resorting to Asset Name because BARRAID and SEDOL are sometimes N/A and and Asset ID may not match.
    active_weight_is_zero = (positions_report['Active Weight (%)']==0)
    not_in_extra_id = reduce(np.bitwise_and, [positions_report[idtype]!=id for id, idtype in extra_ids.items()]) if extra_ids is not None else True
    to_drop = positions_report.loc[active_weight_is_zero & not_in_extra_id]
    positions_report = positions_report.drop(to_drop.index)
    factor_exposures = factor_exposures[~reduce(np.bitwise_or, [factor_exposures[col].isin(to_drop[col][to_drop[col]!='N/A']) for col in id_columns])]
    print('The following stocks were dropped due to bad mapping issues inside barra. They have no weight in the fund or benchmark.\n', to_drop['Asset Name'].values.tolist())

    # correcting for yet another issue where barra does not load some given holding despite supplying it properly for
    # the reupload_portfolio. In this case, it still shows up in positions report since it is run against the bench
    # and that holds this stock, but the factor exposures won't show it since it's not in the reupload_portfolio.
    to_drop1 = positions_report[~reduce(np.bitwise_or, [positions_report[col].isin(factor_exposures[col][factor_exposures[col].notna()]) for col in id_columns])]
    positions_report = positions_report.drop(to_drop1.index)
    print('The following stocks were dropped due to barra not loading them properly.\n', to_drop1['Asset Name'].values.tolist())

    sf_client = snowflake_querier.connect_snowflake(role='prd-risk-analyst-bigwh')

    factor_mapping = snowflake_querier.run_query_return_df(new_query=f"""
        select *
        from prdrisk.risk_analysis.er_macl_factors_map
    """, connection_object=sf_client)

    exposure_columns = factor_exposures.columns[factor_exposures.columns.isin(factor_mapping['Long Horizon Name'])]

    factor_covariance_sf = snowflake_querier.run_query_return_df(new_query=f"""
        select *
        from PRDRISK.RISK_ANALYSIS.GPR_TBL_RAW_MDL_B1_MACL_COVAR
        where analysis_date = (
                select max(analysis_date)
                from PRDRISK.RISK_ANALYSIS.GPR_TBL_RAW_MDL_B1_MACL_COVAR
                where analysis_date <= '{covar_date.strftime('%Y-%m-%d')}'
        ) 
        and factor1 in {tuple(exposure_columns)} and factor2 in {tuple(exposure_columns)}
    """, connection_object=sf_client)

    factor_covariance_sf = factor_covariance_sf.drop('ANALYSIS_DATE', axis=1).pivot(index='FACTOR1', columns='FACTOR2', values='VARCOVAR')
    factor_covariance_sf = pd.DataFrame(factor_covariance_sf.fillna(0).values + factor_covariance_sf.T.fillna(0).values, index=factor_covariance_sf.index, columns=factor_covariance_sf.columns)
    np.fill_diagonal(factor_covariance_sf.values, np.diag(factor_covariance_sf.values) / 2)

    new_ids = id_cleanup(factor_exposures[id_columns].values, positions_report[id_columns].replace('N/A', np.nan).values)

    factor_exposures['Combined ID'] = new_ids[0]
    asset_exposures = factor_exposures.dropna(subset=['Combined ID']).drop_duplicates(subset=['Combined ID']).set_index('Combined ID').loc[new_ids[0], exposure_columns].fillna(0)

    positions_report['Combined ID'] = new_ids[1]
    positions_report = positions_report.dropna(subset=['Combined ID']).drop_duplicates(subset=['Combined ID']).set_index('Combined ID').loc[new_ids[0]]

    exposure_scaling = factor_mapping.drop_duplicates().set_index('Long Horizon Name')['Exposure Scaling'].to_dict()
    if dont_scale_exposure is not None:
        dont_scale_exposure = positions_report[reduce(np.bitwise_or, [positions_report[col].isin(dont_scale_exposure) for col in id_columns])].index
    else:
        dont_scale_exposure = []
    asset_exposures.loc[asset_exposures.index.difference(dont_scale_exposure), :] = asset_exposures.loc[asset_exposures.index.difference(dont_scale_exposure), :].apply(lambda col: col * exposure_scaling[col.name])

    selection_risk = positions_report.reset_index().set_index(['Combined ID', 'Combined ID']).loc[new_ids[0], 'Selection Risk'].unstack(level=1).fillna(0)

    error_keys = asset_exposures.columns.difference(factor_covariance_sf.columns)
    if not error_keys.empty:
        print(error_keys, asset_exposures[reduce(np.bitwise_or, [asset_exposures[col] != 0 for col in error_keys])].empty)
        asset_exposures = asset_exposures.loc[:, ~asset_exposures.columns.isin(error_keys)]

    tiny_factor_map = factor_mapping.drop_duplicates().set_index('Long Horizon Name')['Factor Group'].to_dict()
    asset_curr_exposures = asset_exposures[asset_exposures.columns[asset_exposures.columns.map(tiny_factor_map)=='Currencies']]
    asset_non_curr_exposures = asset_exposures[asset_exposures.columns[asset_exposures.columns.map(tiny_factor_map)!='Currencies']]

    all_factor_covar_matrix = (asset_exposures.loc[positions_report.index] @ factor_covariance_sf.loc[asset_exposures.columns, asset_exposures.columns].fillna(0) @ asset_exposures.loc[positions_report.index].T)
    selection_covar_matrix = selection_risk**2

    common_factor_covar_matrix = (asset_non_curr_exposures.loc[positions_report.index] @ factor_covariance_sf.loc[asset_non_curr_exposures.columns, asset_non_curr_exposures.columns].fillna(0) @ asset_non_curr_exposures.loc[positions_report.index].T)
    currency_covar_matrix = (asset_curr_exposures.loc[positions_report.index] @ factor_covariance_sf.loc[asset_curr_exposures.columns, asset_curr_exposures.columns].fillna(0) @ asset_curr_exposures.loc[positions_report.index].T)

    local_market_covar_matrix = common_factor_covar_matrix + selection_covar_matrix
    total_covar_matrix = all_factor_covar_matrix + selection_covar_matrix

    to_return = {}
    if 'all_factor_covariance' in return_type:
        to_return['all_factor_covariance'] = all_factor_covar_matrix
    if 'factor_covariance' in return_type:
        to_return['factor_covariance'] = factor_covariance_sf.loc[asset_exposures.columns, asset_exposures.columns]
    if 'factor_exposures' in return_type:
        to_return['factor_exposures'] = asset_exposures.loc[positions_report.index]
    if 'specific_covariance' in return_type:
        to_return['specific_covariance'] = selection_covar_matrix.loc[positions_report.index, positions_report.index]
    if 'stock_covariance' in return_type:
        to_return['stock_covariance'] = total_covar_matrix.loc[positions_report.index, positions_report.index]
    if 'common_factor_covariance' in return_type:
        to_return['common_factor_covariance'] = common_factor_covar_matrix.loc[positions_report.index, positions_report.index]
    if 'currency_covariance' in return_type:
        to_return['currency_covariance'] = currency_covar_matrix.loc[positions_report.index, positions_report.index]
    if 'local_market_covariance' in return_type:
        to_return['local_market_covariance'] = local_market_covar_matrix.loc[positions_report.index, positions_report.index]
    if 'positions_report' in return_type:
        to_return['positions_report'] = positions_report

    return to_return


def export_multiple_dates(dates: list[date], bdt_service_client: ServiceClient, export_set_name: str, return_only_report_data: bool = True) -> list:
    """Function that exports a given export set for multiple dates and processes the data into a dataframe immediately.

    Args:
        dates (list[date]): List of dates to export for.
        bdt_service_client (ServiceClient): ServiceClient object returned by barra_login_client.
        export_set_name (str): Export set name. If there are multiple export sets to export for multiple dates, call this function multiple times.
        return_only_report_data (bool, optional): Option to passthrough to run_export_set. Likely going to deprecate in the future. Defaults to True.

    Returns:
        list: Returns data in a list of lists. Each item in the outer list corresponds to one date. Within each date, the first item is a list of dataframes that corresponds to actual export data. The second item is the summary of the export metadata and is a single dataframe.
    """    
    with ThreadPool(processes=len(dates)) as tp:
        export_data = tp.starmap(barra_api_wrapper.run_export_set, zip(repeat(bdt_service_client), repeat(export_set_name), dates, repeat(return_only_report_data)))

    return export_data


def test():
    bdt_service_client = barra_api_wrapper.barra_login_client('../barraone api exploration/credentials.json')

    benchmark_options = {'BenchmarkName': 'MSAWFXUAD', 'BenchmarkOwner': 'SYSTEM'}
    # benchmark_options = {}
    portfolio = ('253113', 'InvPROD')
    portfolio_benchmarks = [(portfolio + ('NONE', benchmark_options))]
    # portfolio_benchmarks += [('253116', 'InvPROD', 'NONE', {})]

    analysisDt = date(2025, 1, 31)

    idtype_override = None
    # idtype_override = {'MASTR1297': 'CUSIP'}

    extra_ids = None
    # extra_ids = {'BDSFG98': 'SEDOL', '5474008': 'SEDOL', 'B4JSTL6': 'SEDOL', '6158709': 'SEDOL', 'BRJ3GY4': 'SEDOL'}
    extra_ids = {
        'LP40103077': 'BARRAID', # ishares MSCI EAFE Value ETF
        # 'LP40204687': 'BARRAID', # Schwab Fundamental EM Eq ETF
        'EQVEMALLCAP': 'COMPOSITEID', # EQV EM All Cap User Composite
        'EQVINTLSMCO': 'COMPOSITEID', # EQV Intl Small Company User Composite
    }

    dont_scale_exposure = None
    # dont_scale_exposure = ['US00900W5554', 'US00900W6214']

    # upload_success = combine_portfolios_reupload(bdt_service_client, portfolio_benchmarks, date(2024, 11, 25), idtype_override=idtype_override, extra_ids=extra_ids)

    # covar_matrix = get_covar_matrix(bdt_service_client, date(2024, 11, 25), portfolio_benchmarks, 'MAC.L', idtype_override=idtype_override, extra_ids=extra_ids)
    returned_objects = get_covar_matrix_advanced(bdt_service_client, analysisDt, portfolio_benchmarks, 'MAC.L', idtype_override=idtype_override, extra_ids=extra_ids, return_type=['all_factor_covariance', 'factor_exposures', 'specific_covariance', 'positions_report'])#, dont_scale_exposure=dont_scale_exposure)
    # covar_matrix.to_excel(r'C:\Users\ramesa1\OneDrive - Invesco\Downloads\covar_matrix.xlsx')

    # reports = export_multiple_dates([date(2024, 3, 22), date(2024, 3, 23)], 'Intl_Smid_Beta_Monthly', return_only_report_data=True)
    pass


if __name__ == '__main__':
    test()
