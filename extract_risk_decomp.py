import pandas as pd
import os


def extract_one_row(path: str, sheet_name: str, columns_to_extract: list, file_list: list = None,
                    row_to_extract: list = None, output_file: str = None, extension: str = '.xlsx', **kwd_args):

    if row_to_extract is None:
        row_to_extract = ['Total Risk']

    if file_list is None:
        file_list = list(os.walk(path))[0][2]

    values = []
    for file_name in file_list:
        print(file_name)
        df = pd.read_excel(os.path.join(path, file_name), sheet_name=sheet_name, index_col=0).\
            loc[row_to_extract, columns_to_extract]
        df.index = [file_name[-(len(extension)+8):-len(extension)]]
        values.append(df)

    df = pd.concat(values)
    df.index = pd.to_datetime(df.index)

    if output_file is None:
        return df
    else:
        df.to_excel(output_file)


def extract_one_column(path: str, sheet_name: str, rows_to_extract: list, column_to_extract: list = None, 
                       file_list: list = None, output_file: str = None, extension: str = '.xlsx', **kwd_args):

    if column_to_extract is None:
        column_to_extract = ['Active Risk']

    if file_list is None:
        file_list = list(os.walk(path))[0][2]

    values = []
    for file_name in file_list:
        print(file_name)
        df = pd.read_excel(os.path.join(path, file_name), sheet_name=sheet_name, index_col=0).\
            loc[rows_to_extract, column_to_extract]
        df.columns = [file_name[-(len(extension)+8):-len(extension)]]
        values.append(df)

    df = pd.concat(values, axis=1).T
    df.index = pd.to_datetime(df.index)

    if output_file is None:
        return df
    else:
        df.to_excel(output_file)


def main():
    path = r'C:\Users\ramesa1\Documents\Work\Main Street funds\Deep Dives\2021 H1\Risk Decomposition reports historical\253125'
    # file_list = list(os.walk(path))[0][2]
    output_file = r'C:\Users\ramesa1\Documents\Work\Main Street funds\Deep Dives\2021 H1\Risk Decomposition reports historical\test.xlsx'

    default_options = {'columns_to_extract': ['Portfolio Risk', 'Benchmark Risk'],
                        'rows_to_extract': ['Common Factor Risk', 'Selection Risk'],
                        'file_list': list(os.walk(path))[0][2][:5],
                       'sheet_name': 'MAC.L Grouping_ARamesh', 'output_file': output_file}
    del default_options['output_file']

    final_frame = extract_one_column(path, **default_options).merge(extract_one_row(path, **default_options), how='outer', left_index=True, right_index=True)
    final_frame.to_excel(output_file)


if __name__ == '__main__':
    main()
